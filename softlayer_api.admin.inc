<?php

/**
 * @file
 * Admin callbacks for CDN CloudFront private files integration
 */

/**
 * Form constructor for the Softlayer API settings form.
 */
function softlayer_api_admin_settings() {
  $form = array();
  $form['security'] = array(
    '#title' => t('Security configuration'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
  );
  $form['security']['softlayer_api_endpoint'] = array(
    '#type' => 'select',
    '#title' => t('Endpoint'),
    '#options' => array(
      'public' => t('Public'),
      'private' => t('Private'),
    ),
    '#default_value' => variable_get('softlayer_api_endpoint', 'public'),
    '#required' => TRUE,
  );
  $form['security']['softlayer_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('softlayer_api_username', ''),
    '#required' => TRUE,
  );
  $form['security']['softlayer_api_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('softlayer_api_apikey', ''),
    '#description' => t('API Key (from SoftLayer Control panel)'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

